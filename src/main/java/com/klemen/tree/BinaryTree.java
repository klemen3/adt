package com.klemen.tree;

public class BinaryTree {

	public Node root;

	public BinaryTree(int data) {
		root = new Node(data);
	}

	public BinaryTree() {
		root = null;
	}
	
	public int treeHeight(Node node) {
		if (node == null) {
			return 0;
		}
			
		return 1 + Math.max(treeHeight(node.left), treeHeight(node.right));
	}
	
	public void addNode(int data) {
		Node node = new Node(data);
		if (root == null) {
			root = node;
		} else {
			Node focusNode = root;
			
			Node parent;
			
			while (true) {
				parent = focusNode;
				
				if (data < focusNode.data) {
					focusNode = focusNode.left;
					
					if (focusNode == null) {
						parent.left = node;
						return;
					}
					
				} else {
					focusNode = focusNode.right;
					
					if (focusNode == null) {
						parent.right = node;
						return;
					}
				}
			}
		}
	}
	
	public void inOrderTraverse(Node focusNode) {
		if (focusNode != null) {
			inOrderTraverse(focusNode.left);
			
			System.out.println(focusNode.data);
			
			inOrderTraverse(focusNode.right);
		}
	}
	
	public void preOrderTraverse(Node focusNode) {
		if (focusNode != null) {
			System.out.println(focusNode.data);
			
			preOrderTraverse(focusNode.left);
			
			preOrderTraverse(focusNode.right);
		}
	}
	
}
