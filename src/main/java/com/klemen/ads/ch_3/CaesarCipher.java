package com.klemen.ads.ch_3;

public class CaesarCipher {

	public char[] encoder = new char[26];
	public char[] decoder = new char[26];
	
	public CaesarCipher(int rotation) {
		for (int k = 0; k < 26; k++) {
			encoder[k] = (char) ('A' + (k + rotation) % 26);
			decoder[k] = (char) ('A' + (k - rotation + 26) % 26);
		}
	}
	
	public String encrypt(String message) {
		return transform(message, encoder);
	}
	
	public String decrypt(String secret) {
		return transform(secret, decoder);
	}
	
	
	private String transform(String original, char[] code) {
		char[] message = original.toCharArray();
		
		// loop through char array
		for (int current = 0; current < message.length; current++) {
			if (Character.isUpperCase(message[current])) {
				
				int difference = message[current] - 'A';
				message[current] = code[difference];
			}
		}
		return new String(message);
	}
	
}
