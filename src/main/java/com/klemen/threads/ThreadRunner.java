package com.klemen.threads;

public class ThreadRunner {

	public void startThreads() throws InterruptedException {

		Runnable r1 = new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 7; i++) {
					System.out.println("Runnable");
					try {
						Thread.sleep(800);
						System.out.println("hhh");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		Thread t1 = new Thread(r1);
		
		
		Thread t2 = new Thread(() -> {
			for (int i = 0; i < 7; i++) {
				System.out.println("lambda!!");
				try {
					Thread.sleep(800);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		

		t1.start();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t2.start();
		
		// wait to end both threads to continue main
		t1.join();
		t2.join();
		
		System.out.println("Bye");

	}

}
