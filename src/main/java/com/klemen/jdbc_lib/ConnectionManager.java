package com.klemen.jdbc_lib;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {

	private String url;
	private String driverName;
	private String username;
	private String password;
	private Connection connection;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private PreparedStatement preparedStatement = null;
	private CallableStatement callableStatement = null;

	private static ConnectionManager single_instance = null;

	public static ConnectionManager getInstance() {

		if (single_instance == null) {
			single_instance = new ConnectionManager();
			System.out.println("new instance created");
		}

		return single_instance;
	}

	// private constructor
	private ConnectionManager() {

	}

	public void init(String url, String driverName, String username, String password) {
		this.url = url;
		this.driverName = driverName;
		this.username = username;
		this.password = password;
	}

	public Connection getConnection() {

		try {
			Class.forName(driverName);
			connection = DriverManager.getConnection(url, username, password);
			connection.setAutoCommit(false);

			System.out.println("connected...");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return connection;
	}

	public void closeConnection() {
		try {
			connection.close();
			System.out.println("connection closed.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet runSelectQuery(String query) {
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultSet;
	}

	public void runUpdateQuery(String query) {
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void runProcedure(String plsql) {

	}

	public void runProcedureSelect(String plsql) {
		try {
			callableStatement = connection.prepareCall(plsql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void commit() {
		try {
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void rollback() {
		try {
			connection.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
