package com.klemen.sort;

public class Sort {

	// bubble sort
	public int[] bubbleSort(int[] array) {
		int length = array.length;
		int temp = 0;
		
		for (int i = 0; i < length - 1; i++) {
			for (int j = 0; j < length - 1 - i; j++) {
				if (array[j] > array[j + 1]) {
					temp = array[j + 1];
					array[j + 1] = array[j];
					array[j] = temp;
				}
			}
		}
		return array;
	}
	
	// selection sort: find miminum and swap with i value
	public int[] selectionSort(int[] array) {
		for (int i = 0; i < array.length; i++) {
			int minimum = i;
			for (int j = i + 1; j < array.length; j++) {
				if (array[j] < array[minimum]) {
					minimum = j;
				}
			}
			int temp = array[minimum];
			array[minimum] = array[i];
			array[i] = temp;
		}
		return array;
	}
	
	// insertion sort: move left and check if i is less than i - 1
	public int[] insertionSort(int[] array) {
		int i, j, key, temp;
		
		
		for (i = 1; i < array.length; i++) {
			key = array[i];
			j = i - 1;
				
			while (j >= 0 && key < array[j]) {
				//swap
				temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
				j--;
			}
		}
		return array;
	}
	
	
}
