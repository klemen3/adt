package com.klemen.sort;

import java.util.Random;

public class QuickSort {

	public void quickSort(int[] array) {
		quickSort(array, 0, array.length - 1);
	}
	
	private void quickSort(int[] array, int low, int high) {
		if (low <= high) {
			int index = partition(array, low, high);
			quickSort(array, low, index - 1);
			quickSort(array, index + 1, high);
		}
	}

	private int partition(int[] array, int low, int high) {
		swap(array, low, getPivot(low, high));
		int border = low + 1;
		for (int i = border; i <= high; i++) {
			if (array[i] < array[low]) {
				swap(array, i, border++);
			}
		}
		swap(array, low, border - 1);
		return border - 1;
	}
	
	private void swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	
	private int getPivot(int low, int high) {
		Random random = new Random();
		int pivot = random.nextInt((high - low) + 1) + low;
		return pivot;
	}
}
