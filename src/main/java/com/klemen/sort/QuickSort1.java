package com.klemen.sort;

import java.util.Random;

public class QuickSort1 {

	// https://www.youtube.com/watch?v=SLauY6PpjW4
	
	public void quickSort(int[] array) {
		quickSort(array, 0, array.length - 1);
	}
	
	public void quickSort(int[] array, int left, int right) {
		if (left >= right) {
			return;
		}
		int pivot = getPivot(left, right);
		int index = partition(array, left, right, pivot);
		quickSort(array, left, index - 1);
		quickSort(array, index, right);
	}
	
	
	private int partition(int[] array, int left, int right, int pivot) {
		while (left <= right) {
			while (array[left] < pivot) {
				left++;
			}
			while (array[right] > pivot) {
				right--;
			}
			if (left <= right) {
				swap(array, left, right);
				left++;
				right--;
			}
		}
		return left;
	}
	
	
	private int[] swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
		return array;
	}
	
	
	private int getPivot(int left, int right) {
		Random random = new Random();
		int pivot = random.nextInt((right - left) + 1) + left;
		return pivot;
	}
	
}
