package com.klemen.sort;

public class Search {

	// binary search for first and last value
	public int[] binarySearch(int[] array, int value) {
		int[] result = new int[2];

		int firstIndex = binaryFirst(array, value);
		result[0] = firstIndex;
		System.out.println("First index: " + firstIndex);

		if (firstIndex != -1) {
			int lastIndex = binaryLast(array, value);
			result[1] = lastIndex;
			System.out.println("Last index: " + lastIndex);
		} else {
			System.out.println("Value not found!");
		}
		return result;
	}

	// find first occurence
	private int binaryFirst(int[] array, int value) {
		int low = 0;
		int high = array.length - 1;

		// default return value
		int result = -1;

		while (low <= high) {
			int mid = (low + high) / 2;

			if (value == array[mid]) {
				result = mid;
				high = mid - 1;
			} else if (value > array[mid]) {
				low = mid + 1;
			} else if (value < array[mid]) {
				high = mid - 1;
			}
		}
		return result;
	}

	// find last occurence
	private int binaryLast(int[] array, int value) {
		int low = 0;
		int high = array.length - 1;

		// default return value
		int result = -1;

		while (low <= high) {
			int mid = (low + high) / 2;

			if (value == array[mid]) {
				result = mid;
				low = mid + 1;
			} else if (value > array[mid]) {
				low = mid + 1;
			} else if (value < array[mid]) {
				high = mid - 1;
			}
		}
		return result;
	}

	// simple binary search
	public void simpleBinarySearchForValue(int[] array, int value) {
		int low = 0;
		int high = array.length - 1;

		while (low <= high) {
			int middle = (high + low) / 2;
			if (array[middle] < value) {
				low = middle + 1;
			} else if (array[middle] > value) {
				high = middle - 1;
			} else {
				System.out.println("\nFound a Match for " + value + " at index " + middle);
				low = high + 1;
			}
		}
	}

	// sequential search; find number of matches
	public int sequentialSearchNum(int[] array, int value) {
		int numberOfResults = 0;
		for (int i = 0; i < array.length; i++) {
			if (value == array[i]) {
				numberOfResults++;
			}
		}
		return numberOfResults;
	}

	// sequential search (boolean)
	public boolean sequentialSearch(int[] array, int value) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == value) {
				return true;
			}
		}
		return false;
	}

	// print array
	public void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print("Index: " + i + " ||");
			System.out.println(array[i]);
		}
	}

}
