package com.klemen.var;

public class Recursion {

	public int getTriangularNum(int number) {
		int triangularNumber = 0;

		while (number > 0) {
			triangularNumber = triangularNumber + number;
			number--;
		}

		return triangularNumber;
	}

	public int getTriangularNum1(int number) {
		int triangularNumber = (number * (number + 1)) / 2;

		return triangularNumber;
	}

	// with recursion
	public int getTriangularNum3(int number) {
		if (number == 1) {
			return 1;
		} else {
			int result = number + getTriangularNum(number - 1);
			return result;
		}

	}
	
	public int factorial(int number) {
		int factorial = 1;
		for (int i = 1; i <= number; i++) {
			factorial = factorial * i;
		}
		return factorial;
	}
	
	
	// with recursion
	public int factorial1(int number) {
		if (number == 1) {
			return 1;
		} else {
			return number * factorial1 (number - 1);
		}
	}

}
