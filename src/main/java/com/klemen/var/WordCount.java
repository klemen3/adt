package com.klemen.var;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class WordCount {

	// mapper
	public void splitWords(String text) throws IOException {
		String[] parts = text.split(" ");

		BufferedWriter writer = new BufferedWriter(new FileWriter(
				new File("/home/klemen/media/DATA/Dropbox/ECLIPSE-PROJECTS/ProjectResources/Hadoop/splitText.txt")));
		int one = 1;
		for (String val : parts) {
			writer.append(val + "," + one + "\n");
		}
		writer.close();
	}

	// reducer
	public void consolidate() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(
				new File("/home/klemen/media/DATA/Dropbox/ECLIPSE-PROJECTS/ProjectResources/Hadoop/splitText.txt")));
		String line;
		int one = 1;
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		while ((line = reader.readLine()) != null) {
			String[] parts = line.split(",");
			if (result.containsKey(parts[0])) {
				result.put(parts[0], result.get(parts[0]) + one);
			} else {
				result.put(parts[0], one);
			}
		}
		reader.close();

		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
				"/home/klemen/media/DATA/Dropbox/ECLIPSE-PROJECTS/ProjectResources/Hadoop/splitTextResult.txt")));
		for (String key : result.keySet()) {
			writer.append(key + "," + result.get(key) + "\n");
		}
		writer.close();

	}

}
