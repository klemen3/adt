package com.klemen.dp.flyweight;

public interface Shape {

	public void draw();
	
	static void testPrint() {
		System.out.println("testPrint");
	}
}
