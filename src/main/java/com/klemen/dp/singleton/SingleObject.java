package com.klemen.dp.singleton;

import java.io.Serializable;

public class SingleObject implements Serializable {

	private static final SingleObject INSTANCE = new SingleObject();
	
	private SingleObject() {
		
	}
	
	public static SingleObject getInstance() {
		return INSTANCE;
	}
	
	// needed for serialization (only works if all fields are transient)
	private Object readResolve() {
		return INSTANCE;
	}
	
	public void leaveTheBuilding() {
		System.out.println("leaving the building");
	}
	
	
}
