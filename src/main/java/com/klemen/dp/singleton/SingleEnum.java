package com.klemen.dp.singleton;

public enum SingleEnum {
	INSTANCE;
	
	public void leaveTheBuilding() {
		System.out.println("leaving the building");
	}
}
