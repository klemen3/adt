package com.klemen.naloge;

import java.util.Arrays;

public class Permutations {

	public void stringPermutations(String str) {
		stringPermutations("", str);
	}

	private void stringPermutations(String prefix, String suffix) {
		int length = suffix.length();
		if (length == 0) {
			System.out.println(prefix);
		} else {
			for (int i = 0; i < length; i++) {
				stringPermutations(prefix + suffix.charAt(i), suffix.substring(0, i) + suffix.substring(i + 1, length));
			}
		}
	}

	
	// https://www.youtube.com/watch?v=IPWmrjE1_MU
	// https://www.byte-by-byte.com/permutations/
	// !!!!!
	public void intPermutations(int[] array, int start) {
		if (start >= array.length - 1) {
			System.out.println(Arrays.toString(array));
			System.out.println();
		} else {
			for (int i = start; i < array.length; i++) {
				array = swap(array, start, i);
				intPermutations(array, start + 1);
				array = swap(array, start, i);
			}
		}
	}

	private int[] swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
		return array;
	}

}
