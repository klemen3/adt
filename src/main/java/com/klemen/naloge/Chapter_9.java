package com.klemen.naloge;

public class Chapter_9 {

	// You are given two sorted arrays, A and B, and A has a large enough buffer at
	// the end
	// to hold B Write a method to merge B into A in sorted order

	public int[] mergeSort(int[] a, int[] b) {

		int[] result = new int[a.length + b.length];
		
		int aCounter = 0;
		int bCounter = 0;
		int resultCounter = 0;

		while (aCounter < a.length && bCounter < b.length) {
			if (a[aCounter] < b[bCounter]) {
				result[resultCounter] = a[aCounter];
				resultCounter++;
				aCounter++;
			} else {
				result[resultCounter] = b[bCounter];
				resultCounter++;
				bCounter++;
			}
		}
		while (bCounter < b.length) {
			result[resultCounter] = b[bCounter];
			resultCounter++;
			bCounter++;
		}
		while (aCounter < a.length) {
			result[resultCounter] = a[aCounter];
			resultCounter++;
			aCounter++;
		}
		return result;
	}
}
