package com.klemen.naloge;

import java.util.Stack;

public class StackRecursion {


	public int factorial1(int number) {
		
		Stack<Integer> stack = new Stack();
		
		int result = 1;
		stack.push(number);
		while (!stack.isEmpty()) {
			int i = stack.pop();
			if (i == 1) {
				return result;
			} else {
				result*= i;
				stack.push(i - 1);		
			}
			
		}
		return result;
		
		
		
		
		
	}
	
	
	public int factorial(int number) {
		if (number == 1) {
			return 1;
		}
		
		return number * factorial (number - 1);
	}
}
