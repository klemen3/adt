package com.klemen.naloge;

public class Naloge {

	public String replace(String str, char ch, char n) {
		StringBuilder builder = new StringBuilder();
		String result;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == ch) {
				builder.append(n);
			} else {
				builder.append(str.charAt(i));
			}
		}
		result = builder.toString();

		return result;
	}

	// fibonacci with reccursion (neučinkovito!)
	public int fibonacci1(int number) {
		if (number == 1 || number == 2) {
			return 1;
		}
		return fibonacci1(number - 1) + fibonacci1(number - 2);
	}

	// fibonacci with loop
	public int fibonacci2(int number) {
		if (number == 1 || number == 2) {
			return 1;
		}
		int n1 = 1, n2 = 1, n3;
		for (int i = 2; i < number; i++) {

			// Fibonacci number is sum of previous two Fibonacci number
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
		}
		return n2; // Fibonacci number
	}

	public void print50() {
		for (int i = 1; i <= 50; i++) {
			if (i % (3 * 5) == 0) {
				System.out.println("FizzBuzz");
			} else if (i % 5 == 0)
				System.out.println("Buzz");
			else if (i % 3 == 0)
				System.out.println("Fizz");
			else
				System.out.println(i);
		}
	}

	// factorial with recursion
	public int factorial(int n) {
		if (n == 1) {
			return 1;
		}
		return n * factorial(n - 1);
	}

	// factorial with loop
	public int factorial1(int n) {
		int result = 1;
		for (int i = n; i > 0; i--) {
			result = result * i;
		}
		return result;
	}

	// check if number is Armstrong number
	public boolean isArmstrong(int number) {
		int temp = number;
		int remainder;
		int sum = 0;

		while (temp > 0) {
			remainder = temp % 10;
			temp = temp / 10;
			sum = sum + remainder * remainder * remainder;
		}

		if (sum == number) {
			return true;
		} else {
			return false;
		}

	}

	public void lihaSodaStevila(int number) {
		for (int i = 1; i < number; i++) {
			if (i % 2 == 0) {
				System.out.println(i + " je sodo število.");
			} else {
				System.out.println(i);
			}
		}
	}

	// check if is a prime number
	public boolean primeNumbers(int number) {
		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public double hypotenuse(double a, double b) {
		return Math.sqrt(a*a + b*b);
	}
	
	public double harmonicNumber(int number) {
		double sum = 0.0;
		for (int i = 1; i <= number; i++) {
			sum = sum + 1.0 / i;
		}
		return sum;
	}
	

	// swap numbers with temp var
	public void swapNumbers(int num1, int num2) {
		int temp = num2;
		num2 = num1;
		num1 = temp;
		System.out.println("Num1: " + num1);
		System.out.println("Num2: " + num2);
	}

	// swap numbers without temp var
	public void swapNumbers1(int num1, int num2) {
		num1 = num1 * num2;
		num2 = num1 / num2;
		num1 = num1 / num2;
		System.out.println("Num1: " + num1);
		System.out.println("Num2: " + num2);
	}

	// find max in array
	public int findMaxInArray(int[] array) {
		int max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}

	// find max in array with recursion
	public int findMaxInArrayRecursion(int[] array, int index, int max) {
		while (index < array.length) {
			if (array[index] > max) {
				max = array[index];
			}
			index++;
			findMaxInArrayRecursion(array, index, max);
		}
		return max;
	}

	// potenca z rekurzijo
	public int potenca(int number) {
		if (number == 0) {
			return 1;
		} else {
			return number * potenca(number - 1);
		}
	}

	// find misssing number in array
	public void findMissingNumber(int[] array) {
		int counter = 0;

		for (int i = 0; i < array.length; i++) {
			if (counter == array[i]) {
				System.out.println(array[i]);
				counter++;
			} else {
				System.out.println("Missing: " + counter);
				counter++;
				i--;
			}
		}
	}

}
