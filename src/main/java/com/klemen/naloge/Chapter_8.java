package com.klemen.naloge;

import java.util.ArrayList;

public class Chapter_8 {

	// 8.2
	public void robot(int n) {
		
		int[][] array = new int[n][n];
		
		for (int i = 0; i < n; i++) {
			array[i][0] = 1;
			array[0][i] = 1;
		}
		
		for (int i = 1; i < n; i++) {
			for (int j = 1; j < n; j++) {
				array[i][j] = array[i - 1][j] + array[i][j - 1];
			}
		}
		int result = array[n - 1][n - 1];
		System.out.println("Result: " + result);
	}
	
	
	// 8.3 DN
	
	
	// 8.5
	
//	Implement an algorithm to print all valid (e g , properly opened and closed) combinations of n-pairs of parentheses
//	EXAMPLE:
//	input: 3 (e g , 3 pairs of parentheses)
//	output: ()()(), ()(()), (())(), ((()))
	
	public void osemPet(int n) {
		int numLeft = n;
		int numRight = n;
		
		while (numLeft >= 0) {
			System.out.println("(");
			numLeft--; 
		}
	}
	

}