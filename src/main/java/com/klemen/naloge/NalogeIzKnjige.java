package com.klemen.naloge;

import java.util.Arrays;

public class NalogeIzKnjige {

	// 1.1
	public String isUnique(String str) {
		char[] ch = str.toCharArray();
		for (int i = 0; i < ch.length; i++) {
			char temp = ch[i];

			for (int j = 0; j < ch.length; j++) {
				if (temp == ch[j] && j != i) {
					return "not unique!";
				}
			}
		}
		return "unique";
	}

	// 1.2 v1
	public String reverseString1(String str) {
		char[] chArray = str.toCharArray();
		int length = chArray.length;
		int halfLength = length / 2;

		for (int i = 0; i < halfLength; i++) {
			char temp = chArray[i];
			chArray[i] = chArray[length - 1 - i];
			chArray[length - 1 - i] = temp;
		}
		return new String(chArray);
	}

	// 1.2 v2
	public String reverseString2(String str) {
		char[] chArray = str.toCharArray();
		char[] result = new char[str.length()];
		int length = chArray.length;

		for (int i = chArray.length - 1; i >= 0; i--) {
			result[length - i - 1] = chArray[i];
		}
		String resultString = String.valueOf(result);
		return resultString;
	}
	
	
	public static void reverseArray(int[] array) {
		int length = array.length;
		
		for (int i = 0; i < length; i++) {
			int temp = array[i];
			array[i] = array[length - 1 - i];
			array[length - 1 - i] = temp;
		}
	}

	// 1.3
	public void removeDuplicates(String str) {
		int length = str.length();
		char[] originalCharArray = str.toCharArray();

		String[] resultStringArray = new String[length];

		// copy chars from original string to charStringArray
		for (int i = 0; i < length; i++) {
			resultStringArray[i] = Character.toString(originalCharArray[i]);
		}

		for (int i = 0; i < length; i++) {
			String temp = resultStringArray[i];
			for (int j = i + 1; j < length; j++) {
				if (temp.equals(resultStringArray[j])) {
					resultStringArray[j] = temp + "!";
				}
			}
		}

		// print result
		for (int k = 0; k < length; k++) {
			System.out.println(resultStringArray[k]);
		}
	}

	// 1.4 v1
	public boolean isAnagram1(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}
		char[] ch1 = s1.toCharArray();
		char[] ch2 = s2.toCharArray();

		Arrays.sort(ch1);
		Arrays.sort(ch2);

		// for (int i = 0; i < ch1.length; i++) {
		// if (ch1[i] != ch2[i]) {
		// return false;
		// }
		// }
		if (Arrays.equals(ch1, ch2)) {
			return true;
		} else {
			return false;
		}
	}

	// 1.4 v2
	public boolean isAnagram2(String word, String anagram) {
		if (word.length() != anagram.length()) {
			return false;
		}
		char[] charFromWord = word.toCharArray();
		for (char c : charFromWord) {
			int index = anagram.indexOf(c);
			if (index != -1) {
				anagram = anagram.substring(0, index) + anagram.substring(index + 1, anagram.length());
			} else {
				return false;
			}
		}
		return anagram.isEmpty();
	}

	// 1.5 replace " " with "%20"
	public String conversion(String str, int length) {

		char[] strChars = str.toCharArray();
		int numSpaces = 0;

		for (int i = 0; i < length; i++) {
			if (strChars[i] == ' ')
				numSpaces++;
		}

		int newLength = length + 2 * numSpaces;
		char[] newChars = new char[newLength];

		for (int i = length - 1; i >= 0; i--) {
			char c = strChars[i];
			if (c != ' ') {
				newChars[i + 2 * numSpaces] = c;
			} else {
				newChars[i + 2 * numSpaces] = '0';
				newChars[i + 2 * numSpaces - 1] = '2';
				newChars[i + 2 * numSpaces - 2] = '%';
				numSpaces--;
			}
		}
		String newString = String.valueOf(newChars);
		return newString;
	}

	// 1.6 rotate !!!!!
	public void rotate(int[][] matrix) {
		int length = matrix.length;
		int[][] result = new int[length][length];

		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result.length; j++) {
				result[length - j - 1][i] = matrix[i][j];
			}
		}
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result.length; j++) {
				System.out.println(result[i][j]);
			}

		}
	}

	// DOMAČA NALOGA!!
	public void nula(int[][] matrix) {

	}

	public boolean erbottlewat(String s1, String s2) {
		String concatenate = s1 + s1;
		System.out.println(concatenate);
		if (concatenate.contains(s2)) {
			return true;
		} else {
			return false;
		}
	}

	public int and(int x, int y) {
		return x * y;
	}

	public int or(int x, int y) {
		if (x + y == 0) {
			return 0;
		} else {
			return 1;
		}
	}

}
