package com.klemen.naloge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Laptop implements Comparable<Laptop> {

	private String name;
	private int price;
	
	public Laptop(String name, int price) {
		this.name = name;
		this.price = price;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "Laptop [name=" + name + ", price=" + price + "]";
	}

	
	@Override
	public int compareTo(Laptop l) {
		return this.name.compareTo(l.name);
	}

	
	// comparator, put in separate class!
//	Laptop l1 = new Laptop("Dell", 1000);
//	Laptop l2 = new Laptop("HP", 1200);
//	Laptop l3 = new Laptop("Lenovo", 1400);
//
//	ArrayList<Laptop> list = new ArrayList<Laptop>();
//	list.add(l1);
//	list.add(l3);
//	list.add(l2);
//
//	Comparator<Laptop> com = new Comparator<Laptop>() {
//
//		@Override
//		public int compare(Laptop l1, Laptop l2) {
//			if (l1.getPrice() > l2.getPrice()) {
//				return 1;
//			} else {
//				return -1;
//			}
//		}
//	};
//
//	Collections.sort(list, com);
}
