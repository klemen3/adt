package com.klemen.naloge;

public class Chapter_10 {

	// chapter 10 uvod
	public int divisibleBy(int a, int b) {
		int max = Math.max(a, b);
		int counter = 2;
		int result = max;

		while (true) {
			if (result % a == 0 && result % b == 0) {
				return result;
			} else {
				result = max * counter;
				counter++;
			}
		}
	}

	// evklidov algoritem
	public int najvecjiSkupniDelitelj(int a, int b) {
		if (a == b) {
			return a;
		} else {
			int max = Math.abs(Math.max(a, b));
			int min = Math.abs(Math.min(a, b));

			int result = max - min;

			return najvecjiSkupniDelitelj(min, result);
		}
	}

	// 10.1 !!!!
	public void basketballHoop() {
		// http://www.mathwords.com/b/binomial_probability_formula.htm

	}

	// 10.1 ???
	public double binomial(int n, int k) {
		if (k == 0 || k == n) {
			return 1;
		}
		return binomial(n - 1, k - 1) + binomial(n - 1, k);
	}

	// 10.2
	// (2^n -2)/2^n

	// 10.3 !!!
	public class Line {
		double epsilon = 0.000001;
		double slope;
		double yintercept;

		public Line(double s, double y) {
			slope = s;
			yintercept = y;
		}

		public boolean intersect(Line line2) {
			return Math.abs(slope - line2.slope) > epsilon || Math.abs(yintercept - line2.yintercept) < epsilon;
		}
	}

	public int fnNegate(int number) {
		return -number;
	}

	public int abs(int number) {
		if (number < 0) {
			return -number;
		} else {
			return number;
		}
	}

	public boolean differentSigns(int a, int b) {
		return ((a < 0 && b > 0) || (a > 0 && b < 0)) ? true : false;
	}

	public int subtract(int a, int b) {
		return a + fnNegate(b);
	}

	// OLEPŠAJ!!
	public int multiply(int a, int b) {
		if (a < b) {
			return multiply(b, a);
		}

		int result = 0;

		for (int i = abs(b); i > 0; --i) {
			result += a;
		}

		if (b < 0) {
			result = fnNegate(result);
		}

		return result;
	}

	// quotient = dividend/divisor
	public int divide(int a, int b) {
		if (b == 0) {
			throw new java.lang.ArithmeticException("Divide by 0.");
		}
		
		int divend;
		int divisor = fnNegate(abs(b));
		int quotient = 0;
		
		for (divend = abs(a); divend >= abs(divisor); divend += divisor) {
			quotient++;
		}
		if (differentSigns(a, b)) {
			quotient = fnNegate(quotient);
		}

		return quotient;
	}

}
