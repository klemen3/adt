package com.klemen.ds;

public class Queue {

	int[] queueArray;
	int size = 0;
	int front = 0;
	int rear = 0;
	
	public Queue(int size) {
		queueArray = new int[size];
	}
	
	public void enQueue(int element) {
		if (isFull()) {
			System.out.println("Queue is full!");
		} else {
			queueArray[rear] = element;
			rear++;
			size++;	
		}
	}
	
	public int deQueue() {
		int element = queueArray[front];
		if (isEmpty()) {
			System.out.println("Queue is empty!");
		} else {
			front++;
			size--;	
		}
		return element;
	}
	
	public void show() {
		System.out.println("Elements: ");
		for (int i = 0; i < size; i++) {
			System.out.println(queueArray[front + i] + " ");
		}
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public boolean isFull() {
		return size == queueArray.length;
	}
	
}