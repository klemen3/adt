package com.klemen.ds;

import java.util.HashSet;

public class LinkedList {

	class Node {
		int element;
		Node next;
	}

	Node head;
	int size = 0;

	public void insert(int element) {
		Node newest = new Node();
		newest.element = element;
		newest.next = null;

		if (head == null) {
			head = newest;
		} else {
			Node n = head;
			while (n.next != null) {
				n = n.next;
			}
			n.next = newest;
		}
		size++;
	}

	public void insertAtStart(int element) {
		Node newest = new Node();
		newest.element = element;
		newest.next = head;
		head = newest;
		size++;
	}

	public void insertAt(int index, int element) {
		Node newest = new Node();
		newest.element = element;

		if (index == 0) {
			insertAtStart(element);
		} else {
			Node n = head;
			for (int i = 0; i < index - 1; i++) {
				n = n.next;
			}
			newest.next = n.next;
			n.next = newest;
		}
		size++;
	}

	public void deleteAt(int index) {
		if (index == 0) {
			head = head.next;
		} else {
			Node n = head;
			Node n1 = null;
			for (int i = 0; i < index - 1; i++) {
				n = n.next;
			}
			n1 = n.next;
			n.next = n1.next;
			n1 = null;
		}
		size--;
	}

	// works on sorted list
	public void deleteDuplicates() {
		if (isEmpty()) {
			return;
		}

		int counter = 0;

		Node n = head;
		Node n1;

		while (n.next != null) {
			if (n.element == n.next.element) {
				n1 = n.next.next;
				n.next = null;
				n.next = n1;
				counter++;
			} else {
				n = n.next;
			}
		}
		size = size - counter;
	}
	
	// works on unsorted list
	public void deleteDuplicatesUnsorted() {
		if (isEmpty()) {
			return;
		}
		
		int counter = 0;
		
		Node n = head;
		Node n1;
		Node duplicate;
		
		while (n.next != null) {
			n1 = n;
			while (n1.next != null) {
				if (n.element == n1.next.element) {
					duplicate = n1.next;
					n1.next = n1.next.next;
					duplicate = null;
					counter++;
				} else {
					n1 = n1.next;
				}
			}
			n = n.next;
		}
		size = size - counter;
	}
	
	public void deleteDuplicatesWithHash() {
		HashSet<Integer> hs = new HashSet<>();
		
		Node current = head;
		Node previous = null;
		
		while (current != null) {
			int currentValue = current.element;
			
			if (hs.contains(currentValue)) {
				previous.next = current.next;
			} else {
				hs.add(currentValue);
				previous = current;
			}
			current = current.next;
		}
	}
	
	public int findElement(int index) {
		Node n = head;

		if (index == 0) {
			return head.element;
		} else if (index == size){
			throw new ArrayIndexOutOfBoundsException();
		}
		
		for (int i = 0; i < index - 1; i++) {
			n = n.next;
		}
		n = n.next;
		int element = n.element;
		return element;
	}
	
	public int numberFromReverseElements() {
		int[] numArray = new int[size - 1];
		
		
		
		Node n = head;
		for (int i = 0; i < size - 1; i++) {
			numArray[i] = n.element;
			n = n.next;
		}
		
		String result = "";
		
		for (int i = numArray.length - 1; i >= 0; i--) {
			result = result + numArray[i];
		}
		System.out.println("String: " + result);
		
		int result1 = Integer.parseInt(result);
		
		return result1;
		
		
	}
	
	
	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void print() {
		Node node = head;

		while (node.next != null) {
			System.out.print(node.element + ", ");
			node = node.next;
		}
		System.out.println(node.element);
	}
}
