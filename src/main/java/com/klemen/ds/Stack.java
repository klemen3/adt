package com.klemen.ds;

public class Stack {

	int[] stackArray;
	int top;
	int size;

	public Stack(int size) {
		stackArray = new int[size];
		top = 0;
		size = 0;
	}

	public void push(int data) {
		if (top == stackArray.length) {
			System.out.println("Stack is full!");
		} else {
			stackArray[top] = data;
			top++;
			size++;
		}
	}

	public int pop() {
		int element = 0;
		if (isEmpty()) {
			System.out.println("Stack is empty.");
		} else {
			top--;
			element = stackArray[top];
			stackArray[top] = 0;
			size--;
		}
		return element;
	}

	public int peek() {
		return stackArray[top - 1];
	}

	public boolean isFull() {
		return top == stackArray.length;
	}

	public boolean isEmpty() {
		return top == 0;
	}

	public int size() {
		return top;
	}

	public void show() {
		for (int n : stackArray) {
			System.out.print(n + " ");
		}
		System.out.println();
	}

}
