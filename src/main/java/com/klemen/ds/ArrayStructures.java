package com.klemen.ds;

public class ArrayStructures {
	
	static int[] theArray = new int[50];
	int arraySize = 10;

	public void generateRandomArray() {
		for (int i = 0; i < arraySize; i++) {
			theArray[i] = (int) (Math.random() * 10) + 10;
		}
	}

	public void printArray() {
		System.out.println("----------");
		for (int i = 0; i < arraySize; i++) {
			System.out.println("| " + i + " | " + theArray[i] + " |");
			System.out.println("----------");
		}
	}

	public int getValueAt(int index) {
		if (index < arraySize) {
			return theArray[index];
		}
		return 0;
	}

	public boolean cotanins(int value) {
		boolean result = false;

		for (int i = 0; i < arraySize; i++) {
			if (value == theArray[i]) {
				result = true;
			}
		}
		return result;
	}

	public void deleteAt(int index) {
		if (index < arraySize) {
			for (int i = index; i < (arraySize - 1); i++) {
				theArray[i] = theArray[i + 1];
			}
		}
		arraySize--;
	}

	public void insert(int value) {
		theArray[arraySize] = value;
		arraySize++;
	}

	public void insertAt(int index, int value) {
		arraySize++;
		for (int i = arraySize - 1; i > index; i--) {
			theArray[i] = theArray[i - 1];
		}
		theArray[index] = value;
	}

	public boolean linearSearchForValue(int value) {
		boolean valueInArray = false;
		int counter = 0;
		String indexes = "";

		for (int i = 0; i < arraySize; i++) {
			if (theArray[i] == value) {
				indexes = indexes + " " + i + " ";
				valueInArray = true;
				counter++;
			}
		}
		System.out.println("Found: " + counter + " matches.");
		System.out.println("Indexes: " + indexes);
		return valueInArray;
	}
}
