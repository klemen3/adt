package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import com.klemen.sort.Search;

public class TestSortSearch {

	Search search = new Search();
	
	@Test
	public void testSequentialSearch() {
		int[] testArray = {1, 2, 3, 4, 5, 19, 50, 8, 0};
		
		assertTrue(search.sequentialSearch(testArray, 1));
		assertTrue(search.sequentialSearch(testArray, 2));
		assertTrue(search.sequentialSearch(testArray, 3));
		assertTrue(search.sequentialSearch(testArray, 4));
		assertTrue(search.sequentialSearch(testArray, 5));
		assertTrue(search.sequentialSearch(testArray, 19));
		assertTrue(search.sequentialSearch(testArray, 50));
		assertTrue(search.sequentialSearch(testArray, 8));
		assertTrue(search.sequentialSearch(testArray, 0));
		assertFalse(search.sequentialSearch(testArray, 48));
		assertFalse(search.sequentialSearch(testArray, 18));
		assertFalse(search.sequentialSearch(testArray, -18));
	}
	
}
