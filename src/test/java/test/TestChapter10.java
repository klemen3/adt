package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import com.klemen.naloge.Chapter_10;

public class TestChapter10 {

	Chapter_10 ch10 = new Chapter_10();
	
	@Test
	public void testNajvecjiSkupniDelitelj() {
		assertEquals(30, ch10.najvecjiSkupniDelitelj(150, 60));
		assertEquals(100, ch10.najvecjiSkupniDelitelj(100, 100));
		assertEquals(15, ch10.najvecjiSkupniDelitelj(45, 15));
		assertEquals(1, ch10.najvecjiSkupniDelitelj(77, 153));
		assertEquals(75, ch10.najvecjiSkupniDelitelj(150, 75));
		assertEquals(0, ch10.najvecjiSkupniDelitelj(0, 0));
		assertEquals(5, ch10.najvecjiSkupniDelitelj(-50, 5));
		assertNotEquals(5, ch10.najvecjiSkupniDelitelj(70, 12));
	}
	
	@Test
	public void testDivide() {
		assertEquals(5, ch10.divide(10, 2));
		assertEquals(5, ch10.divide(15, 3));
		assertEquals(1, ch10.divide(10, 7));
		assertEquals(0, ch10.divide(0, 2));
		assertEquals(-5, ch10.divide(-10, 2));
		assertEquals(-5, ch10.divide(10, -2));
		assertEquals(0, ch10.divide(9, 10));
		assertNotEquals(50, ch10.divide(190, 75));
	}
	
	
}
