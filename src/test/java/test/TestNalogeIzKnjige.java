package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.klemen.naloge.NalogeIzKnjige;

class TestNalogeIzKnjige {

	
	NalogeIzKnjige naloge = new NalogeIzKnjige();

	@Test
	public void testIsAnagram1() {
		assertTrue(naloge.isAnagram1("klemen", "nemelk"));
		assertTrue(naloge.isAnagram1("abde", "aebd"));
		assertTrue(naloge.isAnagram1("", ""));
		assertTrue(naloge.isAnagram1(" ", " "));
		assertFalse(naloge.isAnagram1("hello", "hollo"));
		assertFalse(naloge.isAnagram1("a ", "a"));
		assertFalse(naloge.isAnagram1("bbbb", "b"));
	}
	
	@Test
	public void testIsAnagram2() {
		assertTrue(naloge.isAnagram2("klemen", "nemelk"));
		assertTrue(naloge.isAnagram2("abde", "aebd"));
		assertTrue(naloge.isAnagram2("", ""));
		assertTrue(naloge.isAnagram2(" ", " "));
		assertFalse(naloge.isAnagram2("hello", "hollo"));
		assertFalse(naloge.isAnagram2("a ", "a"));
		assertFalse(naloge.isAnagram2("bbbb", "b"));
	}

}
